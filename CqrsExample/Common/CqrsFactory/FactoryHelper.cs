﻿using System;
using System.Linq;
using System.Reflection;

namespace CqrsExample.Common.CqrsFactory
{
    public static class FactoryHelper
    {
        public static Type GetParticularType(Type type)
        {
            Type concreteType = Assembly.GetExecutingAssembly().GetTypes().FirstOrDefault(
                t => type.IsAssignableFrom(t) && !t.IsAbstract && !t.IsInterface);

            if (concreteType == null)
            {
                throw new Exception($"No handler for query: {type.FullName}");
            }

            return concreteType;
        }

        public static bool HasTypeDefaultConstructor(Type concreteType)
        {
            return concreteType.GetConstructor(Type.EmptyTypes) != null;
        }
    }
}
