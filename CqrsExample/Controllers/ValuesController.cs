﻿using CqrsExample.Base.Mediator;
using CqrsExample.Domains.Units.Commands;
using CqrsExample.Domains.Units.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using CqrsExample.Domains.Units.Commands.UpdateUnit;
using CqrsExample.Domains.Units.Queries.GetUnits;

namespace CqrsExample.Controllers
{
	[Route("api/[controller]")]
	public class ValuesController : Controller
	{
		private readonly IMediator _mediator;
		private readonly IMediatorAsync _mediatorAsync;

		public ValuesController(IMediator mediator, IMediatorAsync mediatorAsync)
		{
			_mediator = mediator;
			_mediatorAsync = mediatorAsync;
		}

		[HttpGet]
		public async Task<IActionResult> Get()
		{
			var getUnitsQuery = new GetUnitsQuery();

			var result = _mediator.Execute<GetUnitsQuery, GetUnitsQueryVm>(getUnitsQuery);
			var resultAsync = await _mediatorAsync.Execute<GetUnitsQuery, GetUnitsQueryVm>(getUnitsQuery);

			var updateUnitCommand = new UpdateUnitCommand();

			_mediator.Execute(updateUnitCommand);
			await _mediatorAsync.Execute(updateUnitCommand);

			return Ok(resultAsync);
		}
    }
}
