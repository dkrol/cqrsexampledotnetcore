﻿namespace CqrsExample.Base.Query
{
    public interface IQueryHandler
    {
    }

    public interface IQueryHandler<TQuery, TResult> : IQueryHandler where TQuery : IQuery
    {
        TResult Handle(TQuery query);
    }
}
