﻿namespace CqrsExample.Base.Query
{
    public interface IQueryBus
    {
        TResult ExecuteQuery<TQuery, TResult>(TQuery query) where TQuery : IQuery;
    }
}
