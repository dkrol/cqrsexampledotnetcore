﻿using System.Threading.Tasks;

namespace CqrsExample.Base.Query
{
    public interface IQueryBusAsync
    {
        Task<TResult> ExecuteQueryAsync<TQuery, TResult>(TQuery query) where TQuery : IQuery;
    }
}
