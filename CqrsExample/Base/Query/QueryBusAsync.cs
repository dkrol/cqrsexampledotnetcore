﻿using CqrsExample.Base.Factory;
using System;
using System.Threading.Tasks;

namespace CqrsExample.Base.Query
{
	public class QueryBusAsync : IQueryBusAsync
	{
		private readonly IQueryHandlersFactory _queryHandlersFactory;

		public QueryBusAsync(IQueryHandlersFactory queryHandlersFactory)
		{
			_queryHandlersFactory = queryHandlersFactory;
		}

		public async Task<TResult> ExecuteQueryAsync<TQuery, TResult>(TQuery query) where TQuery : IQuery
		{
			if (query == null)
			{
				throw new ArgumentNullException();
			}

			var handler = _queryHandlersFactory.GetQueryFactoryAsync<TQuery, TResult>();
			return await handler.HandleAsync(query);
		}
	}
}
