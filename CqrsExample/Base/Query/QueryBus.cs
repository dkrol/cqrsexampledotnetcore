﻿using CqrsExample.Base.Factory;
using System;

namespace CqrsExample.Base.Query
{
	public class QueryBus : IQueryBus
	{
		private readonly IQueryHandlersFactory _queryHandlersFactory;

		public QueryBus(IQueryHandlersFactory queryHandlersFactory)
		{
			_queryHandlersFactory = queryHandlersFactory;
		}

		public TResult ExecuteQuery<TQuery, TResult>(TQuery query) where TQuery : IQuery
		{
			if (query == null)
			{
				throw new ArgumentNullException();
			}

			var handler = _queryHandlersFactory.GetQueryFactory<TQuery, TResult>();
			return handler.Handle(query);
		}
	}
}
