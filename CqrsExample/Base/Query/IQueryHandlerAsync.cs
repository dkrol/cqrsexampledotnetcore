﻿using System.Threading.Tasks;

namespace CqrsExample.Base.Query
{
    public interface IQueryHandlerAsync
    {
    }

    public interface IQueryHandlerAsync<TQuery, TResult> : IQueryHandlerAsync where TQuery : IQuery
    {
        Task<TResult> HandleAsync(TQuery query);
    }
}
