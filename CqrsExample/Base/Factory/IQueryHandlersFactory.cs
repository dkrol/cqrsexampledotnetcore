﻿using CqrsExample.Base.Query;

namespace CqrsExample.Base.Factory
{
	public interface IQueryHandlersFactory
	{
		IQueryHandler<TQuery, TResult> GetQueryFactory<TQuery, TResult>() where TQuery : IQuery;
		IQueryHandlerAsync<TQuery, TResult> GetQueryFactoryAsync<TQuery, TResult>() where TQuery : IQuery;
	}
}
