﻿using CqrsExample.Base.Query;
using System;
using AutoMapper;
using CqrsExample.Common.CqrsFactory;

namespace CqrsExample.Base.Factory
{
	public class QueryHandlersFactory : IQueryHandlersFactory
	{
		private readonly IServiceProvider _serviceProvider;
		private readonly IMapper _mapper;

		public QueryHandlersFactory(IServiceProvider serviceProvider, IMapper mapper)
		{
			_serviceProvider = serviceProvider;
			_mapper = mapper;
		}

		public IQueryHandler<TQuery, TResult> GetQueryFactory<TQuery, TResult>() where TQuery : IQuery
		{
			Type concreteType = FactoryHelper.GetParticularType(typeof(IQueryHandler<TQuery, TResult>));

			return FactoryHelper.HasTypeDefaultConstructor(concreteType)
				? (IQueryHandler<TQuery, TResult>)Activator.CreateInstance(concreteType)
				: (IQueryHandler<TQuery, TResult>)Activator.CreateInstance(concreteType, _serviceProvider, _mapper);
		}

		public IQueryHandlerAsync<TQuery, TResult> GetQueryFactoryAsync<TQuery, TResult>() where TQuery : IQuery
		{
			Type concreteType = FactoryHelper.GetParticularType(typeof(IQueryHandlerAsync<TQuery, TResult>));

			return FactoryHelper.HasTypeDefaultConstructor(concreteType)
				? (IQueryHandlerAsync<TQuery, TResult>)Activator.CreateInstance(concreteType)
				: (IQueryHandlerAsync<TQuery, TResult>)Activator.CreateInstance(concreteType, _serviceProvider,
					_mapper);
		}
	}
}
