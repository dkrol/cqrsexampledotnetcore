﻿using AutoMapper;
using CqrsExample.Base.Command;
using System;
using CqrsExample.Common.CqrsFactory;

namespace CqrsExample.Base.Factory
{
    public class CommandHandlersFactory : ICommandHandlersFactory
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IMapper _mapper;

        public CommandHandlersFactory(IServiceProvider serviceProvider, IMapper mapper)
        {
            _mapper = mapper;
            _serviceProvider = serviceProvider;
        }

        public ICommandHandler<TCommand> GetCommandFactory<TCommand>() where TCommand : ICommand
        {
            Type particularType = FactoryHelper.GetParticularType(typeof(ICommandHandler<TCommand>));

            return FactoryHelper.HasTypeDefaultConstructor(particularType)
                ? (ICommandHandler<TCommand>)Activator.CreateInstance(particularType)
                : (ICommandHandler<TCommand>)Activator.CreateInstance(particularType, _serviceProvider, _mapper);
        }

        public ICommandHandlerAsync<TCommand> GetCommandFactoryAsync<TCommand>() where TCommand : ICommand
        {
            Type concreteType = FactoryHelper.GetParticularType(typeof(ICommandHandlerAsync<TCommand>));

            return FactoryHelper.HasTypeDefaultConstructor(concreteType)
                ? (ICommandHandlerAsync<TCommand>)Activator.CreateInstance(concreteType)
                : (ICommandHandlerAsync<TCommand>)Activator.CreateInstance(concreteType, _serviceProvider, _mapper);
        }

    }
}
