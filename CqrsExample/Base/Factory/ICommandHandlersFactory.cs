﻿using CqrsExample.Base.Command;

namespace CqrsExample.Base.Factory
{
    public interface ICommandHandlersFactory
    {
        ICommandHandler<TCommand> GetCommandFactory<TCommand>() where TCommand : ICommand;
        ICommandHandlerAsync<TCommand> GetCommandFactoryAsync<TCommand>() where TCommand : ICommand;
    }
}
