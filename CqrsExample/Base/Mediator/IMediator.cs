﻿using CqrsExample.Base.Command;
using CqrsExample.Base.Query;

namespace CqrsExample.Base.Mediator
{
	public interface IMediator
    {
        void Execute<TCommand>(TCommand command) where TCommand : ICommand;
        TResult Execute<TQuery, TResult>(TQuery query) where TQuery : IQuery;
    }
}
