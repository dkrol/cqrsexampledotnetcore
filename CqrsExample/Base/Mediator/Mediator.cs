﻿using CqrsExample.Base.Command;
using CqrsExample.Base.Query;

namespace CqrsExample.Base.Mediator
{
    public class Mediator : IMediator
    {
        private readonly IQueryBus _queryBus;
        private readonly ICommandBus _commandBus;

        public Mediator(IQueryBus queryBus, ICommandBus commandBus)
        {
            _queryBus = queryBus;
            _commandBus = commandBus;
        }

        public void Execute<TCommand>(TCommand command) where TCommand : ICommand
        {
            _commandBus.ExecuteCommand<TCommand>(command);
        }

        public TResult Execute<TQuery, TResult>(TQuery query) where TQuery : IQuery
        {
            return _queryBus.ExecuteQuery<TQuery, TResult>(query);
        }
    }
}
