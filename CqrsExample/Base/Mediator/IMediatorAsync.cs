﻿using System.Threading.Tasks;
using CqrsExample.Base.Command;
using CqrsExample.Base.Query;

namespace CqrsExample.Base.Mediator
{
	public interface IMediatorAsync
	{
		Task Execute<TCommand>(TCommand command) where TCommand : ICommand;
		Task<TResult> Execute<TQuery, TResult>(TQuery query) where TQuery : IQuery;
	}
}
