﻿using System.Threading.Tasks;
using CqrsExample.Base.Command;
using CqrsExample.Base.Query;

namespace CqrsExample.Base.Mediator
{
	public class MediatorAsync : IMediatorAsync
	{
		private readonly IQueryBusAsync _queryBusAsync;
		private readonly ICommandBusAsync _commandBusAsync;

		public MediatorAsync(IQueryBusAsync queryBusAsync, ICommandBusAsync commandBusAsync)
		{
			_queryBusAsync = queryBusAsync;
			_commandBusAsync = commandBusAsync;
		}

		public async Task Execute<TCommand>(TCommand command) where TCommand : ICommand
		{
			await _commandBusAsync.ExecuteCommandAsync<TCommand>(command);
		}

		public async Task<TResult> Execute<TQuery, TResult>(TQuery query) where TQuery : IQuery
		{
			return await _queryBusAsync.ExecuteQueryAsync<TQuery, TResult>(query);
		}
	}
}
