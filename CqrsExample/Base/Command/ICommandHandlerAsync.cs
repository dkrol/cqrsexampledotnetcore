﻿using System.Threading.Tasks;

namespace CqrsExample.Base.Command
{
    public interface ICommandHandlerAsync
    {
    }

    public interface ICommandHandlerAsync<TCommand> : ICommandHandlerAsync where TCommand : ICommand
    {
        Task HandleAsync(TCommand command);
    }
}
