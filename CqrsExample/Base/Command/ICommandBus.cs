﻿namespace CqrsExample.Base.Command
{
    public interface ICommandBus
    {
        void ExecuteCommand<TCommand>(TCommand command) where TCommand : ICommand;
    }
}
