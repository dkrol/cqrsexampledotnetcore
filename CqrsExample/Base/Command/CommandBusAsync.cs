﻿using CqrsExample.Base.Factory;
using System;
using System.Threading.Tasks;

namespace CqrsExample.Base.Command
{
    public class CommandBusAsync : ICommandBusAsync
    {
        private readonly ICommandHandlersFactory _commandHandlersFactory;

        public CommandBusAsync(ICommandHandlersFactory commandHandlersFactory)
        {
            _commandHandlersFactory = commandHandlersFactory;
        }

        public async Task ExecuteCommandAsync<TCommand>(TCommand command) where TCommand : ICommand
        {
            if (command == null)
            {
                throw new ArgumentNullException();
            }

            var handler = _commandHandlersFactory.GetCommandFactoryAsync<TCommand>();
            await handler.HandleAsync(command);
        }
    }
}
