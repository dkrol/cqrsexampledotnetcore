﻿using CqrsExample.Base.Factory;
using System;

namespace CqrsExample.Base.Command
{
    public class CommandBus : ICommandBus
    {
        private readonly ICommandHandlersFactory _commandHandlersFactory;

        public CommandBus(ICommandHandlersFactory commandHandlersFactory)
        {
            _commandHandlersFactory = commandHandlersFactory;
        }

        public void ExecuteCommand<TCommand>(TCommand command) where TCommand : ICommand
        {
            if (command == null)
            {
                throw new ArgumentNullException();
            }

            var handler = _commandHandlersFactory.GetCommandFactory<TCommand>();
            handler.Handle(command);
        }
    }
}
