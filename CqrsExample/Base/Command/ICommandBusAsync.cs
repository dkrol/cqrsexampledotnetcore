﻿using System.Threading.Tasks;

namespace CqrsExample.Base.Command
{
    public interface ICommandBusAsync
    {
        Task ExecuteCommandAsync<TCommand>(TCommand command) where TCommand : ICommand;
    }
}
