﻿using AutoMapper;
using CqrsExample.Base.Command;
using CqrsExample.Base.Factory;
using CqrsExample.Base.Mediator;
using CqrsExample.Base.Query;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CqrsExample
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureAutoMapper(services);
            ConfigureCustomServices(services);
            services.AddMvc();
        }

        private void ConfigureCustomServices(IServiceCollection services)
        {
            services.AddScoped<IMediator, Mediator>();
            services.AddScoped<IMediatorAsync, MediatorAsync>();
            services.AddScoped<IQueryBus, QueryBus>();
            services.AddScoped<ICommandBus, CommandBus>();
            services.AddScoped<IQueryBusAsync, QueryBusAsync>();
            services.AddScoped<ICommandBusAsync, CommandBusAsync>();
            services.AddScoped<IQueryHandlersFactory, QueryHandlersFactory>();
            services.AddScoped<ICommandHandlersFactory, CommandHandlersFactory>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }

        private void ConfigureAutoMapper(IServiceCollection services)
        {
            services.AddAutoMapper();
        }
    }
}
