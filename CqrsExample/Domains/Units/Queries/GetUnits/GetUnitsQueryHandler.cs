﻿using CqrsExample.Base.Query;

namespace CqrsExample.Domains.Units.Queries.GetUnits
{
    public class GetUnitsQueryHandler : IQueryHandler<GetUnitsQuery, GetUnitsQueryVm>
    {
        public GetUnitsQueryVm Handle(GetUnitsQuery query)
        {
            var result = new GetUnitsQueryVm();
            result.Name = "TEST QUERY";

            return result;
        }
    }
}
