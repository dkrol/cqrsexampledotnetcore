﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CqrsExample.Base.Query;

namespace CqrsExample.Domains.Units.Queries.GetUnits
{
    public class GetUnitsQueryHandlerAsync : IQueryHandlerAsync<GetUnitsQuery, GetUnitsQueryVm>
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IMapper _mapper;

        public GetUnitsQueryHandlerAsync(IServiceProvider serviceProvider, IMapper mapper)
        {
            _serviceProvider = serviceProvider;
            _mapper = mapper;
        }

        public async Task<GetUnitsQueryVm> HandleAsync(GetUnitsQuery query)
        {
            var result = new GetUnitsQueryVm();
            result.Name = "TEST QUERY ASYNC";

            return await Task.Run(() => result);
        }
    }
}
