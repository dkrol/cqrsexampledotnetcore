﻿using CqrsExample.Base.ViewModel;

namespace CqrsExample.Domains.Units.Queries.GetUnits
{
    public class GetUnitsQueryVm : IViewModel
    {
        public string Name { get; set; }
    }
}
