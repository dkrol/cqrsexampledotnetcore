﻿using CqrsExample.Base.Command;

namespace CqrsExample.Domains.Units.Commands.UpdateUnit
{
    public class UpdateUnitCommandHandler : ICommandHandler<UpdateUnitCommand>
    {
        public void Handle(UpdateUnitCommand command)
        {
            // Implementation of particular handler for particular command !
        }
    }
}
