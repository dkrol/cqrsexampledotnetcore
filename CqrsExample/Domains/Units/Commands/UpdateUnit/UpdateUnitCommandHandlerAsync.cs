﻿using System;
using System.Threading.Tasks;
using CqrsExample.Base.Command;

namespace CqrsExample.Domains.Units.Commands.UpdateUnit
{
    public class UpdateUnitCommandHandlerAsync : ICommandHandlerAsync<UpdateUnitCommand>
    {
        public async Task HandleAsync(UpdateUnitCommand command)
        {
            // Implement handle async
            await Task.Run(() => Console.WriteLine("test"));
        }
    }
}
