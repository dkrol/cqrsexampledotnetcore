﻿using CqrsExample.Base.Command;

namespace CqrsExample.Domains.Units.Commands.UpdateUnit
{
    public class UpdateUnitCommand : ICommand
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
